# tretton37

tretton37 code assignment

## Stack

Built with React, TypeScript and MUI

## Installation

Requires `npm` and `Node.js` (tested with v14.18.0)

- clone the repo
- In the root directory, create a file called `.env` with the content: `REACT_APP_AUTH_HEADER=<tretto37-auth-header>`\*
- `npm install`
- `npm start`
- Navigate to `http://localhost:3000`

_\*The header name itself ("Authorization:") should be omitted from this string_

## User stories

| design/accessibility | functionality               | testing/QA     |
| -------------------- | --------------------------- | -------------- |
| Responsive design    | Sort by name and office     | Use Typescript |
|                      | Filter by name and office   |                |
|                      | Enable switch between views |                |
