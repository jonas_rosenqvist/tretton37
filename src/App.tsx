import {
  Box,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Container,
  CssBaseline,
  Grid,
  Typography,
  IconButton,
  InputLabel,
  FormControl,
  MenuItem,
  Select,
  TextField,
  ToggleButton,
  ToggleButtonGroup,
  Hidden,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Button,
} from "@mui/material";
import TwitterIcon from "@mui/icons-material/Twitter";
import GitHubIcon from "@mui/icons-material/GitHub";
import LinkedInIcon from "@mui/icons-material/LinkedIn";
import AppsIcon from "@mui/icons-material/Apps";
import MenuIcon from "@mui/icons-material/Menu";
import EmailIcon from "@mui/icons-material/Email";
import { useEffect, useState } from "react";
import { Employee, getEmployees } from "./API";

type SortType = "name" | "name_desc" | "location" | "location_desc";
const PAGE_SIZE = 48;

export const App = () => {
  const [state, setState] = useState<{
    error?: string;
    employees?: Employee[];
    loading: boolean;
  }>({ loading: true });
  const [pageSize, setPageSize] = useState<number>(PAGE_SIZE);
  const [sortBy, setSortBy] = useState<SortType>("name");
  const [filter, setFilter] = useState<string>("");
  const [viewMode, setViewMode] = useState<"table" | "grid">("grid");
  useEffect(() => {
    const fetchEmployees = async () => {
      const employees = await getEmployees();
      if (employees.error) {
        setState({
          error: employees.error,
          employees: undefined,
          loading: false,
        });
        return;
      }
      if (employees.data) {
        setState({
          error: undefined,
          employees: employees.data,
          loading: false,
        });
      }
    };
    fetchEmployees();
  }, []);

  /**
   * Comparator used to sort employees given the selected sorting.
   */
  const sorter = (a: Employee, b: Employee): number => {
    switch (sortBy) {
      case "name":
        return a.name.localeCompare(b.name);
      case "name_desc":
        return -1 * a.name.localeCompare(b.name);
      case "location":
        return a.office.localeCompare(b.office);
      case "location_desc":
        return -1 * a.office.localeCompare(b.office);
    }
    return 0;
  };

  /**
   * Filter used to filter employees given the selected filter.
   */
  const filterer = (e: Employee) => {
    if (!e.published) {
      return false;
    }
    if (!filter) {
      return true;
    }
    return (
      e.name.toLowerCase().includes(filter) ||
      e.office.toLocaleLowerCase().includes(filter)
    );
  };

  /**
   * Return a sorted list of all visible employees, based on current filter, sorting and page size.
   */
  const getCurrentEmployees = () => {
    return (state.employees || [])
      .filter(filterer)
      .sort(sorter)
      .slice(0, pageSize);
  };

  /**
   * Renders all visible employees as cards
   */
  const renderCards = () => {
    return (
      <Grid container spacing={2} mb={5}>
        {getCurrentEmployees().map((employee) => {
          return (
            <Grid key={employee.name} item xs={12} md={6} lg={4}>
              <Card elevation={4}>
                <Box sx={{ display: "flex" }}>
                  <CardMedia
                    component="img"
                    image={employee.imagePortraitUrl}
                    sx={{ width: 100 }}
                    alt={`Portrait picture`}
                  />
                  <Box sx={{ width: "100%" }}>
                    <CardContent>
                      <Typography
                        sx={{
                          minHeight: "3em",
                          fontSize: "1.2em",
                          color: "#2e7d32",
                        }}
                      >
                        {employee.name}
                      </Typography>
                      <Typography>Located in {employee.office}</Typography>
                    </CardContent>

                    <CardActions
                      sx={{ display: "flex", justifyContent: "right" }}
                    >
                      <IconButton
                        href={`https://www.twitter.com/${employee.twitter}`}
                        target="_blank"
                      >
                        <TwitterIcon />
                      </IconButton>
                      <IconButton
                        target="_blank"
                        href={`https://www.linkedin.com/${employee.linkedIn}`}
                      >
                        <LinkedInIcon />
                      </IconButton>
                      <IconButton
                        target="_blank"
                        href={`https://github.com/${employee.gitHub}`}
                      >
                        <GitHubIcon />
                      </IconButton>
                      <IconButton
                        target="_blank"
                        href={`mailto:${employee.email}`}
                      >
                        <EmailIcon />
                      </IconButton>
                    </CardActions>
                  </Box>
                </Box>
              </Card>
            </Grid>
          );
        })}
      </Grid>
    );
  };
  /**
   * Renders a table, and all visible employees as table rows
   */
  const renderTable = () => {
    return (
      <TableContainer component={Paper}>
        <Table stickyHeader>
          <TableHead>
            <TableRow>
              <TableCell>NAME</TableCell>
              <TableCell>LOCATION</TableCell>
              <Hidden mdDown>
                <TableCell>PHONE</TableCell>
                <TableCell>EMAIL</TableCell>
              </Hidden>
              <TableCell>SOCIAL MEDIA</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {getCurrentEmployees().map((employee) => (
              <TableRow key={employee.name}>
                <TableCell>{employee.name}</TableCell>
                <TableCell>{employee.office}</TableCell>
                <Hidden mdDown>
                  <TableCell>{employee.phoneNumber}</TableCell>
                  <TableCell>{employee.email}</TableCell>
                </Hidden>
                <TableCell>
                  <Box sx={{ display: "flex" }}>
                    <IconButton
                      href={`https://www.twitter.com/${employee.twitter}`}
                      target="_blank"
                    >
                      <TwitterIcon />
                    </IconButton>
                    <IconButton
                      target="_blank"
                      href={`https://www.linkedin.com/${employee.linkedIn}`}
                    >
                      <LinkedInIcon />
                    </IconButton>
                    <IconButton
                      target="_blank"
                      href={`https://github.com/${employee.gitHub}`}
                    >
                      <GitHubIcon />
                    </IconButton>
                  </Box>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    );
  };
  if (state.loading) {
    return (
      <CssBaseline>
        <Container sx={{ mt: 1 }}>Loading, please wait..</Container>
      </CssBaseline>
    );
  }
  if (state.error) {
    return (
      <CssBaseline>
        <Container sx={{ mt: 1 }}>
          There was an error fetching employee information: {state.error}
        </Container>
      </CssBaseline>
    );
  }
  return (
    <CssBaseline>
      <Container sx={{ mb: 5 }}>
        <Box
          sx={{
            display: "flex",
            gap: "1em",
            alignItems: "center",
            flexWrap: "wrap",
            mt: 4,
            mb: 2,
          }}
        >
          <h2 style={{ margin: "0" }}>Tretton37</h2>
          <Box>
            <FormControl>
              <InputLabel id="sort-select">Sort by</InputLabel>
              <Select
                labelId="sort-select"
                value={sortBy}
                label="Sort by"
                onChange={(e) => setSortBy(e.target.value as SortType)}
                sx={{ width: "22ch", mr: 2 }}
                size="small"
              >
                <MenuItem value={"name"}>Name</MenuItem>
                <MenuItem value={"name_desc"}>Name (desc.)</MenuItem>
                <MenuItem value={"location"}>Location</MenuItem>
                <MenuItem value={"location_desc"}>Location (desc.)</MenuItem>
              </Select>
            </FormControl>
            <TextField
              size="small"
              sx={{ width: "22ch" }}
              value={filter}
              onChange={(e) => setFilter(e.target.value)}
              label="Filter (name or city)"
            />
          </Box>
          <Hidden smDown>
            <ToggleButtonGroup
              value={viewMode}
              exclusive
              onChange={(_, value) => setViewMode(value)}
            >
              <ToggleButton size="small" title="Grid view" value="grid">
                <AppsIcon />
              </ToggleButton>
              <ToggleButton size="small" title="Table view" value="table">
                <MenuIcon />
              </ToggleButton>
            </ToggleButtonGroup>
          </Hidden>
        </Box>
        {viewMode === "grid" ? renderCards() : renderTable()}
        {pageSize < (state.employees || []).filter(filterer).length && (
          <Box
            sx={{
              display: "flex",
              justifyContent: "right",
              mt: 3,
              alignItems: "center",
            }}
          >
            Displaying {pageSize} / {(state.employees || []).length}
            <Button
              sx={{ ml: 1 }}
              onClick={() => setPageSize(pageSize + PAGE_SIZE)}
            >
              Load more
            </Button>
          </Box>
        )}
      </Container>
    </CssBaseline>
  );
};
