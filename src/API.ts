export interface Employee {
  name: string;
  email: string;
  phoneNumber: string;
  office: string;
  gitHub: string;
  twitter: string;
  stackOverflow: string;
  linkedIn: string;
  imagePortraitUrl: string;
  published: boolean;
}
export interface APIResponse<T> {
  data?: T;
  error?: string;
}

/**
 * Fetches all employees
 * @returns an `APIResponse` with a list of `Employee`  in `data`. If an error occured, data will be undefined and  `error` will be truthy instead.
 */
export const getEmployees = async (): Promise<APIResponse<Employee[]>> => {
  try {
    const auth = process.env.REACT_APP_AUTH_HEADER;
    if (!auth) {
      return { error: "Authorization token is missing" };
    }
    const resp = await fetch(`https://api.1337co.de/v3/employees`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: auth,
      },
    });
    if (!resp.ok) {
      return {
        error:
          resp.status > 500
            ? "There's something wrong with the tretton37 server"
            : resp.status === 401 || resp.status === 403
            ? "It seems like the authenticaton token is invalid"
            : `Some other error. Status code ${resp.status }`,
      };
    }
    const data = await resp.json();
    return { data };
  } catch (error) {
    return {
      error: "Network error, please check your internet connection",
    };
  }
};
